<?php

use App\Person;


function __autoload($className){
    list($ns,$cn) = explode("\\",$className);
    require_once ("../../src/BITM/SEIP123456/".$cn.".php");
}


$obj = new Person();

echo $obj->showPersonInfo();
